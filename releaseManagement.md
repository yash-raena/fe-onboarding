# Raena - Release Management #

## Release Branch Name ##

When a release is planned, first thing required is to select the version number and create a release branch <br />

The following logic is proposed for the choosing the release branch name: <br />

* Check if there is a release branch already present, and if not present, pick the current app version.
* Let's assume that the version found in the previous step is `7.3.x`
* If the planned release will require a force update, do a major version bump. So it will be `8.0.0`
* If the planned release has new features but it will not be a force update, a minor version bump will be fine. `7.4.0`
* Release branch name format will be `release/v{releaseVersion}`. For example - `release/v7.4.0`

## Updating Release Branch ##

Each release branch will have a <b>branch owner</b> who will be reponsible for keeping it upto date and in sync with versions lower than it. <br />
Release branch owner should be comfortable in using `git rebase`. <br />

Lets discuss a scenario where two release branches are in development. <br />

* A project usually has the following branches `master`, `staging` and `develop`. (There will be some exceptions)
* Let's assume that it has two release branches `release/v7.3.0` and `release/v7.4.0` with owners in different pods.
* Each morning, it will be responsiiblity of the owners to update the release branch.
* The following commands can be used.
```
a. For release/v7.3.0
- git checkout develop
- git pull origin develop
- git checkout release/v7.3.0
- git rebase -i develop (resolve conflicts if any)
- git push -f origin release/v7.3.0
```

* And similarly
```
b. For release/v7.4.0
- git checkout release/v7.3.0
- git pull origin release/v7.3.0
- git checkout release/v7.4.0
- git rebase -i release/v7.3.0 (resolve conflicts if any)
- git push -f origin release/v7.4.0
```

## Updating feature branch ##

* Each developer working on his feature branch can run the following commands to take the latest changes.
```
- git checkout {releaseBranch}
- git fetch
- git reset --hard origin/{releaseBranch}
This will update the release branch on your local system.
- git checkout {featureBranch}
- git rebase -i {releaseBranch}
```

## Hot fixes ##

* PR for hot fixes will be raised against <b>develop</b> branch.


## Merging Release Branch ##

* Release branch PR will be raised against develop.
* Branch owner should ensure that all the commits following the [conventional commits format](https://www.conventionalcommits.org/en/v1.0.0/)
* Branch owner should also ensure that there are no redundant merge commits and commit history is clean. (While merging any feature PR, please use the squash opeion to achieve this)
* If above two points are not found, whole release will be squashed in a single commit.

## Branch Merge strategies ##

* feature/hotfix branch to release/develop -> squash
* release branch to develop -> fast forward
* develop to staging -> fast forward
* staging to master -> fast forward


## Branch ownership ##
Branch owner will be responsible for merging the changes in the specified branch

* release branches - Individual Pod will assign a person for this task
* develop/staging/master - Yash
