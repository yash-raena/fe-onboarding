# Raena - Frontend Onboarding #

### Development Environment setup ###

* Node - 14.x
* NPM - 6.x
* Docker

### Repositories ###

* Mobile App - https://bitbucket.org/engineering_raena/raena-mobile-app
* Admin UI - https://bitbucket.org/engineering_raena/raena-admin-ui
* Reseller SAAS - https://bitbucket.org/engineering_raena/raena-connect-web
* Category And Brands Pages - https://bitbucket.org/engineering_raena/raena.com
* Dispatcher App - https://bitbucket.org/engineering_raena/raena-dispatcher-app
* Digital Store Front - https://bitbucket.org/engineering_raena/digital-store-front/

### Development Workflow ###

* [Git Best Practices](./gitBestPractices.md)
* [Release Management](./releaseManagement.md)
* To be added...

